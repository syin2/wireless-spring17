# README #

This repository was created for future reference on a sample wireless related lecture in introduction to wireless 
communication courses. The materials can be catered to introduction to programming as well. There are live demos during
the lecture and student will be highly motivated for the live demo continuously. 

### What is this repository for? ###

* The repository contains all materials that can be used in a lecture for undergraduate in Computer Science.
* v1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Demo 1: Connecting to access point ###

* Devices:
    - Laptop 1: running UBUNTU 16.04, Teamviewer 
    - Laptop 2: HDMI port, Teamviewer 
    - Wireless AP1: Buffalo router, password=nonenone 
    - Wireless AP2: Buffalo router, password=nonenone 
    - wifi usb dongle: ar9271 
* Summary of set up :  
we need 2 access points that can be set up and used as two ssids. One laptop with a usb wifi dongle to be used to approach near and away from the ssid. A live plot will be shown in the class there should be another laptop that has been connected to the hdmi of the projector. It can remote access the moving laptop. 
* Configuration: 
  The script named demo1. 
* Dependencies: 
  The script depends on the python-networkmanager. It needs to be run under Ubuntu16.04
* Database configuration: 
* How to run tests: 
```
  cd demo1-connap 
  python wifiplot.py
```
* Deployment instructions: 
  Both laptop needs to install teamviewer.
  
### Demo 2: Light based communication ###

* Devices: 
    - laptop 1: running opal vm 
    - telosb 
    - usb cable longer one 
* Summary of set up: 
one telosb mote with light sensors has to be used and mounted on the side of a door frame. It needs a long USB
  cable to be connected with laptop for projection. There should be a flashlight that is blinking every one second.
* Configuration: 
* Dependencies: 
  The script depends on Tinyos. We have make it used in Opal virtual image.
* How to run tests: 
```
  cd demo2-lightcom 
  python dump.py /dev/ttyUSB0
```
* Deployment instructions: 
  Make sure the classroom's projector has HDMI connections.
  

### Demo3: visible light communication ###

* Devices:
    - laptop1: to connect to bbb board
    - BBB : we need two, one as TX, the other as RX
    - USB cables: two short one and two longer one
    - a panel: for fixing BBB
    - LED: make sure the LED on the cape is inserted
* Summary of set up:
  We need to align two BBB boards and make sure the distance between TX and   RX is no longer than 50cm. This is important since large distance will      sacrifice performance, causing demo failure. Make sure the board is fixed   as well, especially when we carry the panel with the board to the           classroom. Caution needs to be taken care of. 
* Configuration:
  It takes time (around 30s or longer) for the laptop to recognize the board. So the longer usb cable was to extend the short USB cable so that the laptop can be connected with HDMI for the projector while the vlc panel can be faced to the audience for demonstration.
* Dependencies: 
    - Hardware: It has to be purple 1.0 or openvlc 1.0 cape
    - Software: It has to be openvlc.
* How to run tests:
  for RX:
```
  ssh machinekit@192.168.17.2 
  PS1="Alice: " 
  cd Alice/alice-demo 
  python receive.py & 
  ./monitor.sh
```
  The monitor script will display file size increase in real time. 
  for TX:
```
  ssh machinekit@192.168.7.2 
  PS1="Bob: " 
  cd Bob/bob-demo 
  python send.py 
```
  for laptop: open a browser
```
  192.168.17.2:8080/out.jpeg 
```
  The audience will see a picture turning from half into whole.

* Deployment Instructions:
  Make sure the board is connected before the demo so that audience does 
  not need to wait for the board connection.


### Demo4: sensing ###
* Devices: 
  telosb: 3 (2 for radio based sensing and 1 for light based sensing)
  laptop: running opal vm 
* Summary of set up: all 3 telosb motes were mount on the side of door frame. 
* Configurations: 1 telosb needs to be programmed using sendingMote, 1 telosb needs to be flashed using RssiBase and 1 telosb needs to be flashed with Sampler (read sensor values from telosb mote)
* Dependencies: 
  Opal vm.
* How to rum test: 
  for light based sensing:
```
  cd demo2-lightcomm/Sampler 
  make telosb install,/dev/ttyUSB0 
  python plot.py /dev/ttyUSB0 
```
  for radio based sensing:
```
  cd demo4-sensing/RssiDemo/sendingmote 
  make 
  cd demo4-sensing/RssiDemo/RssiBase 
  make 
  cd demo4-sensing/RssiDemo/java-demo 
  make  
  java RssiDemo -comm Serial@/dev/ttyUSB0:telos 
```

### Improvement todolists ##

* TX and RX needs to be clear when doing demo3: vlc
* Hessam: Have a real people counting application. I mean instead of a graph have a counter which increases and decreases. 
* Hessam: Try to have some sort of human identification. something like differentiating just 2 persons from each other based on the signal. 
* Hessam: Have an alarm system. Once someone passes through the door, the system raises an alarm sound.
* Om: Think of improvement without adding new features.