###Make###

Run as root,

```
sudo -s
make telosb
```

###Make install###
Connect the USB to the laptop throught a long USB cable

```
motelist
make telosb install, /dev/ttyUSB*
```

###Generate the live plot for light sensor readings###

```
python plot.py /dev/ttyUSB*
```

###Change sampling frequency###

Check the header file.
